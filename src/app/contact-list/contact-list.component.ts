import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ContactService } from '../contact.service';
import { IContacts } from '../contact';
import { Router, ActivatedRoute } from '@angular/router';
declare var UIkit: any;

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  contacts: IContacts[];
  selectedContact: IContacts;

  constructor(private contactService: ContactService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.contactService.list().then((data) => {
      this.contacts = data;
    });

    UIkit.util.on('#formmodal', 'hide', () => {
      this.selectedContact = undefined;
    });

  }

  edit(data: IContacts) {
    this.selectedContact = data;

    console.log(this.selectedContact);
    console.log(typeof this.selectedContact);
  }


  delete(id: number) {
    this.contacts = this.contacts.filter(contact => contact.id !== id);
    this.contactService.delete(id).then(() => {
      this.contacts.splice(id, 1);
    });
  }

}
