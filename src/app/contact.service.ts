import { Injectable } from '@angular/core';
import { IContacts } from './contact';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take, first, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ContactService {
  url = "http://localhost:3000";

  constructor(private http: HttpClient) { }

  /* postApiData() {
    return this.http.post('http://localhost:3000/data', { firstName: 'Divya', lastName: 'Dave' });
  } */
  /*  nameData() {
     return this.http.get('http://localhost:3000/getData');
   } */
  add(contact: IContacts) {
    /* console.log(typeof contact); */
    return this.http.post('http://localhost:3000/addContact', contact).pipe(first()).toPromise();
  }
  list() {
    return this.http.get('http://localhost:3000/list').pipe(first(), map(e => e as IContacts[])).toPromise();
    /*  console.log(e);
     console.log(e as IContacts[]); */
  }
  edit(contact: IContacts) {
    console.log(contact);
    return this.http.get<IContacts[]>(`${this.url}/edit/${contact.id}`).pipe(first()).toPromise();
  }
  delete(id) {
    return this.http.delete(`${this.url}/delete/${id}`).pipe(first()).toPromise();
  }
}
