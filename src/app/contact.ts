export interface IContacts {
    id: number;
    firstName: string;
    lastName: string;
    company: string;
    jobTitle: string;
    email: string;
    phone: number;
    notes: string;
}