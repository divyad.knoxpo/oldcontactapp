import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators, FormGroup } from '@angular/forms';
import { phoneNumberValidator } from '../validators/phone-validator';
import { ContactService } from '../contact.service';
import { IContacts } from '../contact';
declare var UIkit: any;



@Component({
  selector: 'app-contactform',
  templateUrl: './contactform.component.html',
  styleUrls: ['./contactform.component.scss']
})
export class ContactformComponent implements OnInit, OnChanges {

  contactForm: FormGroup;
  submitted: boolean = false;
  isEditForm: boolean = false;
  @Input() editData: IContacts;
  //contact: Contacts;
  h1Style: boolean = false;

  constructor(private fb: FormBuilder, private contactService: ContactService) { }

  ngOnInit() {
    this.initForm();
    UIkit.util.on('#formmodal', 'show', () => {
      this.editDataCheck();
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.editData.currentValue !== changes.editData.previousValue) {
      if (this.editData) {

        this.editDataCheck();

      }
    }
  }

  initForm() {
    this.contactForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      company: ['', Validators.required],
      jobTitle: [''],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [phoneNumberValidator]],
      notes: ['']
    });

    this.editDataCheck();
  }

  private editDataCheck() {
    this.contactForm.reset();
    if (this.editData) {
      this.isEditForm = true;
      this.contactForm.setValue({
        firstName: this.editData.firstName,
        lastName: this.editData.lastName,
        company: this.editData.company,
        jobTitle: this.editData.jobTitle,
        email: this.editData.email,
        phone: this.editData.phone,
        notes: this.editData.notes
      });
    }
  }

  get phone() {
    return this.contactForm.controls.phone;
  }
  get email() {
    return this.contactForm.controls.email;
  }
  reset() {
    this.submitted = false;
    this.contactForm.reset();
  }
  save() {
    /* this.contactService.addContact(this.contactForm).subscribe((res) => {
      console.log(res);
    }); */
  }
  onSubmit() {
    this.contactService.add(this.contactForm.value).then((res) => {
      console.log(this.contactForm.value);
      console.log(res);
    });
    this.h1Style = true;
  }
}
