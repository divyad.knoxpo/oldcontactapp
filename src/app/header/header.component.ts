import { Component, OnInit, Input } from '@angular/core';
import { trigger, transition, state, animate, style, AnimationEvent } from '@angular/animations';

@Component({
  selector: 'app-header',
  animations: [
    trigger('openClose', [
      state('open', style({
        transform: 'translate(0)'
      })),
      state('close', style({
        transform: 'translate(-250px)'
      })),
      transition('open=>close', animate('1s')),
      transition('close=>open', animate('1s'))
    ]),
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() sidenav;

  constructor() { }

  ngOnInit() {
  }

}
